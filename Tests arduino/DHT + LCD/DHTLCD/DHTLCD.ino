#include <SimpleDHT.h>
#include <LiquidCrystal.h>

#define DHTPIN 23

SimpleDHT11 dht11(DHTPIN);

const int rs = 48, en = 49, d4 = 50, d5 = 51, d6 = 52, d7 = 53;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup()
{
  lcd.begin(16, 2);
  lcd.print("Initializing...");
  delay(2000);
}

void loop()
{
  lcd.clear();
  AmbientData();
  delay(5000);
  lcd.clear();
}

void AmbientData()
{
  byte humidity, temperature;
  
  int check = dht11.read(&temperature,&humidity, NULL);
  DisplayLCDInfos((int)temperature, (int)humidity);
}

void DisplayLCDInfos(int temp, int hum)
{
  lcd.setCursor(0,0);
  lcd.print("Temp : ");
  lcd.print(temp);
  lcd.print(" C");
  lcd.setCursor(0, 1);
  lcd.print("Humidity : ");
  lcd.print(hum);
  lcd.print("%");
}

void DisplayErrorLCD()
{
  lcd.setCursor(0, 0);
  lcd.print("Read error");
}

