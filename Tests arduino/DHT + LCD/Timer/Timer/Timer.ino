#include <Timer.h>

Timer t;

void TimerFunc()
{
  digitalWrite(6, HIGH);
  delay(1000);
  digitalWrite(6, LOW); 
}

void setup() 
{
  Serial.begin(9600);
  pinMode(6, OUTPUT);
  t.every(5000, TimerFunc, NULL);
}

void loop() 
{
  Serial.println("Turn");
  delay(1000);
  t.update();
}

