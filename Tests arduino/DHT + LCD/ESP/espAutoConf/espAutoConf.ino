#include <SoftwareSerial.h>

SoftwareSerial esp(11, 12);

int moistureTreshold;

String net = "VOO-015716";
String pw = "MJBXPCTG";

void setup() 
{
  Serial.begin(9600);
  moistureTreshold = 20;
  esp.begin(115200); 
}

void loop() 
{
  
  /*serialRead();
  delay(5000);
  Serial.print("Moisture limit : ");
  Serial.println(moistureTreshold);*/
}

void serialRead()
{
  String cmd = "";
  
  while(Serial.available() > 0)
  {
    char reading = Serial.read();
    cmd.concat(reading);
  }

  if(cmd.startsWith("M"))
  {
    changeMoistureLimit(cmd);
    Serial.println("OK");
  }

  if(cmd.startsWith("W"))
  {
    connectEsp();
  }
}

void changeMoistureLimit(String cmd)
{
  int newTreshold = cmd.substring(2).toInt();
  Serial.print("new tresh :");
  Serial.println(newTreshold);
  moistureTreshold = newTreshold;
}

void sendToEsp(String cmd)
{
  esp.println(cmd);
}

String receiveFromEsp(const int timeout)
{
  String answer = "";
  long int tm = millis();
  
  while((tm+timeout) > millis())
  {
    while(esp.available())
    {
      char c = esp.read();
      answer += c;
    }
  }
  
  Serial.print(answer);  
}

