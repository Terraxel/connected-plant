int moisture = 0;

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  int hum = analogRead(A0);
  int fnRead = map(hum, 0, 1023, 100, 0);
  Serial.print(fnRead);
  Serial.print(" / ");
  Serial.println(hum);
  delay(1000);
}
