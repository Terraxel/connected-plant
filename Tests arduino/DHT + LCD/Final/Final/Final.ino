//Création complète par DUDZIAK THOMAS
//Projet de BAC 2 Informatique
//Plante connectée

#include <LiquidCrystal.h> //Bibliothèque permettant de gérer l'écran LCD
#include <SimpleDHT.h> //Gestion du capteur d'humidité et de température de l'air
#include <SoftwareSerial.h> //Connexion au module WIFI ESP8266

/*
  NB : J'ai besoin de la dernière Bibliothèque car j'utilise déjà la liaison série physique afin de 
  recevoir des commandes d'un programme externe réalisé en C#.NET
*/

#define DHTPIN 23 //Pin de données du capteur d'humidité et de température de l'air
#define REDLED 45 //Pin de la DEL rouge d'alerte
#define GREENLED 44 //Pin de la DEL verte signifiant des conditions correctes
#define PUMPPIN 42 //Pin de la pompe à eau

const int rs = 48, en = 49, d4 = 50, d5 = 51, d6 = 52, d7 = 53;
//Définition des pins de l'écran LCD avec leurs noms respectifs

int moistureLimit = 20; //Seuil d'humidité du sol à ne pas dépasser
int currentAmbientMoisture = 0, currentAmbientTemp = 0, currentSoilMoisture = 0;
//Les conditions mesurées en temps réel seront stockées dans ces 3 variables

LiquidCrystal lcd(rs, en, d4, d5, d6, d7); //Création d'un objet LiquidCrystal pour gérer l'écran LCD
SimpleDHT11 dht11(DHTPIN);
//Création d'un objet SimpleDHT11 me permettant de gérer le module de détection d'humidité et de température de l'air ambiant

void setup() 
{
  pinMode(REDLED, OUTPUT);
  pinMode(GREENLED, OUTPUT);
  pinMode(PUMPPIN, OUTPUT);
  //On définit les pins des DEL en sortie ainsi que le pin vers le transistor contrôlant la pompe

  Serial.begin(9600); //Début de la liaison série physique
  LCDInit(); //Appel de la fonction permettant d'initialiser l'écran LCD
  delay(2000); //On attend 2 secondes pour faire genre car c'est cool
  //InitESP + ESPAnswer LATER
  //TimerPOSTRequest LATER
}

void loop()
{
  ReadSerialCmd(); //On lit la liaison physique série pour voir si l'on reçoit des instructions du programme C#.NET
  ReadSensorValues(); //On lit les valeurs des capteurs (Humidité du sol & humidité + température air ambiant)
  LCDDisplayInfos(); //On affiche tout ça sur l'écran
  MoistureAction(); //On agit en conséquence des résultats lus
  delay(5000); //On attends 5 secondes avant de répéter toutes ces actions
  //updateTimer LATER //On met à jour le timer pour les reqûetes vers le serveur
}

void LCDInit()
{
  lcd.begin(16, 2); //On a un écran 16x2
  lcd.clear(); //On clear l'écran
  lcd.setCursor(0,0); //On place le curseur au début
  lcd.write("Initializing..."); //On dit qu'on initialise tout le système
}

void MoistureAction()
{
  if(currentSoilMoisture < moistureLimit)
  {
    digitalWrite(GREENLED, LOW);
    digitalWrite(REDLED, HIGH);
    Watering();
  }
  else
  {
    digitalWrite(GREENLED, HIGH);
    digitalWrite(REDLED, LOW);
  }

  /*
    Si on est en dessous du seuil d'humidité programmé, allume la DEL rouge et on étéint la verte et on démarre l'arrosage.
    Dans le cas échéant on allume la DEL verte et on éteint la rouge. Evidemment, on n'arrose pas.
  */
}

void LCDDisplayInfos()
{
  lcd.clear(); //On clear l'écran
  lcd.setCursor(0,0); //On se place au début en haut
  lcd.print("Air : ");
  lcd.print(currentAmbientTemp);
  lcd.print(" C /");
  lcd.print(currentAmbientMoisture);
  lcd.print("%");
  //On affiche : "Air : {température_air} C / {humidité_air}%"

  lcd.setCursor(0,1); //On se place en deuxième rangée
  lcd.print("Soil : ");
  lcd.print(currentSoilMoisture);
  lcd.print("%/");
  lcd.print(moistureLimit);
  lcd.print("%");
  //On affiche : "Soil : {humidité_sol}%/{humidité_sol_MAX}%"
}

void ReadSerialCmd()
{
  String cmd = ""; //On crée une variable où stocker notre commande reçue (s'il y en a une)
  
  while(Serial.available() > 0)
  {
    char reading = Serial.read();
    cmd.concat(reading);
    //Tant que l'on a quelque chose à lire sur la liaison série, on le lit et on l'ajoute à notre variable cmd qui stocke notre commande
  }

  if(cmd.startsWith("M"))
  {
    ChangeMoistureLimit(cmd);
  }
  else if(cmd.startsWith("W"))
  {
    //Connexion ESP LATER
  }

  //Si c'est une commande reçue pour changer le seuil d'humidité ou une commande pour se connecter à un réseau WIFI, on le fait.
  //Si la commande n'est pas valide, on ne fait rien.
}

void ChangeMoistureLimit(String cmd)
{
  //Notre commande ressemble à : "M={nouvelle_valeur}"

  int newTreshold = cmd.substring(2).toInt(); //On veut juste l'entier qui fait office de nouvelle valeur
  moistureLimit = newTreshold; //On affecte lla nouvelle 
  Serial.print("New moisture treshold : "); 
  Serial.println(moistureLimit);
  //On renvoie le changement avec la valeur sur la liaison série pour un éventuel traitement par le programme C#.NET externe
}

void ReadSensorValues()
{
  int soil = analogRead(A0); //On lit la valeur de tension sur le pin ayant le détecteur d'humidité de la terre
  currentSoilMoisture = map(soil, 0, 1023, 100, 0); //On convertit notre valeur d'une échelle 0->1023 vers une échelle 0->100

  byte humidity, temperature;  
  int check = dht11.read(&temperature,&humidity, NULL); //On lit notre température et humidité (on remarquera la référence vers les "byte" créés au dessus)
  currentAmbientTemp = (int)temperature;
  currentAmbientMoisture = (int)humidity; //On cast(e) car on veut les stocker en tant qu'entiers
}

void Watering()
{
  lcd.clear(); //On clear l'écran
  lcd.setCursor(0,0); //On se place au début
  lcd.print("Watering..."); //On indique qu'on va arroser
  
  while(currentSoilMoisture < 30 || currentSoilMoisture < moistureLimit) //Tant qu'on est en dessous du seuil d'humidité + 15%
  {
    digitalWrite(PUMPPIN, HIGH); //On initie une tension sur la grille du transistor, on allume donc la pompe
    ReadSensorValues(); //On lit les valeurs des capteurs pour les actualiser (en fait, l'humidité de la terre nous importe)
    delay(1000); //On réitère chaque seconde
  }

  digitalWrite(PUMPPIN, LOW); //Une fois sorti de la boucle, l'arrosage est terminé. On fait donc en sorte que le MOSFET ne soit plus passant.

  lcd.setCursor(0, 1); 
  lcd.print("End of watering"); //On indique que l'arrosage est terminé sur la deuxième ligne

  //NB : Après quelques secondes, le système reviendra à son état de fonctionnement dans des conditions "correctes"
}


