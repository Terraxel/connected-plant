
var context = document.getElementById("barChart")
var contextTwo = document.getElementById("polarAreaChart")

var myChart = new Chart(context, {
    type: 'bar',
    data: 
    {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: 
        [{
            label: '# of Votes',
            data: [12, 22, 3, 5, 2, 3],
            backgroundColor: 
            [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: 
            [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: 
    {
        scales: 
        {
            yAxes: 
            [{
                ticks: 
                {
                    beginAtZero:true
                }
            }]
        }
    }
});

var myRadarChart = new Chart(contextTwo, {
    type: 'polarArea',
    data: 
    {
        labels: ["Rouge", "Bleu", "Vert"],
        datasets:
        [{
            label: 'Mes couleurs',
            data : [27, 36, 42],
            backgroundColor :
            [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(106, 214, 56, 0.6)' 
            ],
            borderColor :
            [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(106, 214, 56, 1)'          
            ],
            borderWidth : 1
        }]
    },
    options: 
    {
        scales: 
        {
            yAxes: 
            [{
                ticks: 
                {
                    beginAtZero:true
                }
            }]
        }
    }
});