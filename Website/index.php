<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Plant manager</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap and custom stylesheet import -->

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="main.css" />

</head>

<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-success sticky-top" style="height: 9vh;">

      <a class="nav navbar-brand" href="#">
        <img src="https://www.countryfinancial.com/content/dam/cfin/bands/icons/icon_history_2008_plant-01.png" width="50" height="50" alt="Logo">
        <span style="margin: 3px;">Plant Manager</span>
      </a>

        <ul class="nav navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link active" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Doc</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">A</a>
          </li>
        </ul>

  </nav>


  <div class="container-fluid head-image">
    <div class="row">
      <div class="col-md-6 top-left">
        <h1 class="white-text impact-font" style="font-size: 50px;">Easy plant management</h1>
        <p class="text-center">
          <ul class="no-itemMarker white-text" style="padding-top: 20px; font-size:20px;">
            <li><i class="fas fa-angle-right"></i> Vizualize a historic of your plant data</li>
            <li><i class="fas fa-angle-right"></i> Intuitive graphs</li>
            <li><i class="fas fa-angle-right"></i> Easy to use</li>
          </ul>
        </p>
      </div>
      <div class="col-xs-1 text-center bottom">
        <button type="button" onclick="onGoDownClick()" class="btn btn-light circular-btn"><i class="fas fa-angle-double-down"></i></button>
      </div>
    </div>
  </div>


  <div id="part2" class="container-fluid" style="height: 100vh; background-color:rgba(115, 215, 240, 0.2);">

  </div>


  <!-- <script src="graph.js"></script> -->
  <script src="index.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
  <script src="https://unpkg.com/simplebar@latest/dist/simplebar.js"></script>
</body>

</html>
