﻿namespace Plant_Device_Manager
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.wifiBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ssidTb = new System.Windows.Forms.TextBox();
            this.pwTb = new System.Windows.Forms.TextBox();
            this.wifiProgress = new System.Windows.Forms.ProgressBar();
            this.moistureBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.moistureTb = new System.Windows.Forms.TextBox();
            this.moistureProgress = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.comTb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // wifiBtn
            // 
            this.wifiBtn.Location = new System.Drawing.Point(12, 110);
            this.wifiBtn.Name = "wifiBtn";
            this.wifiBtn.Size = new System.Drawing.Size(260, 48);
            this.wifiBtn.TabIndex = 0;
            this.wifiBtn.Text = "Connect Device to Network";
            this.wifiBtn.UseVisualStyleBackColor = true;
            this.wifiBtn.Click += new System.EventHandler(this.wifiBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "SSID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password :";
            // 
            // ssidTb
            // 
            this.ssidTb.Location = new System.Drawing.Point(58, 50);
            this.ssidTb.Name = "ssidTb";
            this.ssidTb.Size = new System.Drawing.Size(214, 22);
            this.ssidTb.TabIndex = 3;
            // 
            // pwTb
            // 
            this.pwTb.Location = new System.Drawing.Point(95, 82);
            this.pwTb.Name = "pwTb";
            this.pwTb.Size = new System.Drawing.Size(177, 22);
            this.pwTb.TabIndex = 4;
            this.pwTb.UseSystemPasswordChar = true;
            // 
            // wifiProgress
            // 
            this.wifiProgress.Location = new System.Drawing.Point(12, 164);
            this.wifiProgress.Name = "wifiProgress";
            this.wifiProgress.Size = new System.Drawing.Size(260, 23);
            this.wifiProgress.TabIndex = 5;
            // 
            // moistureBtn
            // 
            this.moistureBtn.Location = new System.Drawing.Point(322, 110);
            this.moistureBtn.Name = "moistureBtn";
            this.moistureBtn.Size = new System.Drawing.Size(161, 48);
            this.moistureBtn.TabIndex = 6;
            this.moistureBtn.Text = "Change Moisture Treshold";
            this.moistureBtn.UseVisualStyleBackColor = true;
            this.moistureBtn.Click += new System.EventHandler(this.moistureBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(327, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Minimum plant moisture :";
            // 
            // moistureTb
            // 
            this.moistureTb.Location = new System.Drawing.Point(322, 80);
            this.moistureTb.Name = "moistureTb";
            this.moistureTb.Size = new System.Drawing.Size(161, 22);
            this.moistureTb.TabIndex = 8;
            // 
            // moistureProgress
            // 
            this.moistureProgress.Location = new System.Drawing.Point(322, 164);
            this.moistureProgress.Name = "moistureProgress";
            this.moistureProgress.Size = new System.Drawing.Size(161, 23);
            this.moistureProgress.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(197, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "COM";
            // 
            // comTb
            // 
            this.comTb.Location = new System.Drawing.Point(242, 12);
            this.comTb.Name = "comTb";
            this.comTb.Size = new System.Drawing.Size(72, 22);
            this.comTb.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 202);
            this.Controls.Add(this.comTb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.moistureProgress);
            this.Controls.Add(this.moistureTb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.moistureBtn);
            this.Controls.Add(this.wifiProgress);
            this.Controls.Add(this.pwTb);
            this.Controls.Add(this.ssidTb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.wifiBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Device Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button wifiBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ssidTb;
        private System.Windows.Forms.TextBox pwTb;
        private System.Windows.Forms.ProgressBar wifiProgress;
        private System.Windows.Forms.Button moistureBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox moistureTb;
        private System.Windows.Forms.ProgressBar moistureProgress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox comTb;
    }
}

