﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace Plant_Device_Manager
{
    class SerialCom : IDisposable
    {
        SerialPort connection { get; set; }

        public SerialCom(string com, int baudRate)
        {
            this.connection = new SerialPort(com, baudRate);
            this.connection.DataReceived += OnDataReceived;
            this.connection.Open();
        }

        private void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var sPort = (SerialPort)sender;

            string data = sPort.ReadLine();
        }

        public SerialCom() : this("COM4", 9600)
        {
            
        }

        public void SendMessage(string message)
        {
            this.connection.WriteLine(message);
        }

        public void Dispose()
        {
            this.connection.Close();
            this.connection.Dispose();
        }
    }
}
