﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plant_Device_Manager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void wifiBtn_Click(object sender, EventArgs e)
        {
            string comPort = GetComPort();
            string ssid = this.ssidTb.Text;
            string pwd = this.pwTb.Text;

            using (var com = new SerialCom(comPort, 9600))
            {
                com.SendMessage($"W=\"{ssid}\",\"{pwd}\"");
            }
        }

        private void moistureBtn_Click(object sender, EventArgs e)
        {
            string comPort = GetComPort();
            string moistureTreshold = this.moistureTb.Text;

            this.moistureProgress.Value = 50;

            using (var com = new SerialCom(comPort, 9600))
            {
                com.SendMessage($"M={moistureTreshold}");
            
            }

            this.moistureProgress.Value = 100;
        }

        private string GetComPort()
        {
            return $"COM{this.comTb.Text}";
        }
    }
}
